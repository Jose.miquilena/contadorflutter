
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{

  final estiloTexto = new TextStyle(fontSize: 25);

  final conteo = 0;



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Prueba Contador"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(children: <Widget>[
        Text('Número de clicks:', style: estiloTexto),
        Text('$conteo', style: estiloTexto),
      ], mainAxisAlignment: MainAxisAlignment.center,)
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon( Icons.add ),
        onPressed: (){

         
          print('Hola Mundo!');

        },),
      );
  }

}